import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import TabBar from './TabBar'
import ProfileScreen from '../Class/ProfileScreen'
import DashBoard from '../Class/DashBoard'
import Streams from '../Class/Streams';
import ReportedTweets from '../Class/reportedTweets';

export default class App extends Component {

  render() {
    return (
      <View style={styles.container}>
          <TabBar>
              <TabBar.Item
                icon={require('../assests/home_icon_twitter.png')}
                selectedIcon={require('../assests/home_icon_twitter.png')}
                title="Home"
              >
                <View style={styles.textContent}>
                  <DashBoard navigation={this.props.navigation} />
                  {/* <Text style={{fontSize: 18}}>Home</Text> */}
                </View>
               
              </TabBar.Item>


              <TabBar.Item
                icon={require('../assests/notification.png')}
                selectedIcon={require('../assests/notification.png')}
                title="Streams"
              >
                <View style={styles.textContent}>
                  <Streams navigation={this.props.navigation} />
                    {/* <Text style={{fontSize: 18}}>Home</Text> */}
                </View>

              </TabBar.Item>

              
              <TabBar.Item>
                <View style={styles.textContent}>
                <DashBoard navigation={this.props.navigation} />
                    {/* <Text style={{fontSize: 18}}>Friend</Text> */}
                </View>
              </TabBar.Item>

            <TabBar.Item
                icon={require('../assests/report.png')}
                selectedIcon={require('../assests/report.png')}
                title="Reported Tweets"
                // Reported Tweets
              >
                <View style={styles.textContent}>
                  <ReportedTweets navigation={this.props.navigation} />
                </View>
              </TabBar.Item>



              <TabBar.Item
                icon={require('../assests/male.png')}
                selectedIcon={require('../assests/male.png')}
                title="Profile"
              >
                <View style={styles.textContent}>
                    <ProfileScreen navigation={this.props.navigation} />
                    {/* <Text style={{fontSize: 18}}>Me</Text> */}
                </View>
              </TabBar.Item>
          </TabBar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      flexDirection:"row",
      justifyContent:"flex-end",
      alignItems: 'center',
      // backgroundColor : 'pink',
  },
  textContent: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
  }
});