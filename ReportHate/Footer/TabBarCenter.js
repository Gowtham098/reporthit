import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions
} from 'react-native';
export default class TabBarCenter extends Component {
  render() {
    const style = this.props.setBtn == 3 ? styles.centerView : styles.centerViewBtn;
    return (
     
        <View style={[style,styles.eqView]}>
            {/* <Text style={styles.centerViewText}>u</Text> */}
            <Image style={{height:35,width:35}} source={require('../assests/feather_white.png')} />
        </View>
    );
  }
}
const styles = StyleSheet.create({
    eqView: {
        width: 60,
        height:60,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor:"#1DA1F2",
        borderRadius:30,
        elevation : 5,
        bottom:0,
        shadowColor: '#000000',
        shadowOffset: {
        width: 0,
        height: 3,
        },
        shadowRadius: 5,
        shadowOpacity: 0.4, 
    },
    centerView: {
        position: "absolute",
        left: Dimensions.get('window').width/2,
        bottom:15,
        right:0,
        marginLeft:-30,        
    },
    centerViewBtn: {
        position: "absolute",
        bottom:15,  
    },
    centerViewText: {
        color: "#fff",
        fontSize: 34,
    },
});
