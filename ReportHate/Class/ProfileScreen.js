import React, { Component } from 'react';
import {
    StyleSheet,
    FlatList,
    View,
    StatusBar,
    Text,
    Button,
    Image,
    ImageBackground,
    ActivityIndicator,
    Modal,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    I18nManager
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Fonts } from './utils/fonts'
import { TextInput } from 'react-native-paper';

import Constant from './Constant'

var FloatingLabel = require('react-native-floating-labels');

import LinearGradient from 'react-native-linear-gradient'

let SCREEN_HEIGHT = Dimensions.get('window').height;
let SCREEN_WIDTH = Dimensions.get('window').width;

export default class ProfileScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ProfileArray : [],
            isBool: false,

            isDiscount: false,
            oldPassword : '',
            newPassword : '',
            confirmPassword: '',
            inputColor: '',
            inputColor1: '',
            isLoading:false,

            // oldPassword : 'webiz@123',
            // newPassword : 'webiz_123',

            // National_ID : global.userData.iqama_NationalID,
        }
    }

    componentDidMount()
    {        
        // if (this.props.navigation.state.params) {
        //     let ProfileData = this.props.navigation.state.params.ProfileData
        //     // alert(JSON.stringify(ProfileData))
        //     // this.setState({ ProfileArray: ProfileData })
        //     // alert(JSON.stringify(this.state.ProfileArray))
        // }
        // alert(global.userData.iqama_NationalID)
        this.getUserData()
    }

  
    clickDropdownViewPercentage =() =>
    {
      // console.log(index)
      this.setState({isDiscount:true})
    }

    touchtohideDiscount()
    {
      this.setState({isDiscount:false,})
    }

    getUserData()
    {
      const _this=this
       Constant.userDb().find({}, function (err, docs)
       {
        // alert(JSON.stringify(docs))
         _this.setState({ProfileArray : docs[0]})
        //  alert(JSON.stringify(_this.state.ProfileArray))
        //  console.log("cashout",docs)
      });  
    }

    navigate()
    {
        this.props.navigation.navigate('LoginScreen', {  }) 
    }

    render() {
       let ProfileData = global.userData
    //    console.log(JSON.stringify(ProfileData))
        return (
            // <ScrollView>
                <SafeAreaView style = {{flex: 1}}>
                <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />

                <View style={{ flex: 1,width: SCREEN_WIDTH,height: SCREEN_HEIGHT, backgroundColor: '#F5F5F5', flexDirection: 'column', backgroundColor: '#ccc' }}>
                    <View style={{ flex: 1, backgroundColor: '#F4F5F6' }}>

                        <View style={{ backgroundColor: '#F4F5F6' }}>
                        </View>
                        <View style={{ flex: 0.3, backgroundColor: '#F4F5F6' }}>


                            {/* <Image source={require('../assests/hospital2.png')} style={{ flex: 1, height: 300, width: '100%', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}  ></Image> */}

                            <ImageBackground
                                style={{ flex: 1, height: 200, width: '100%' }}
                                imageStyle={{ borderBottomLeftRadius: 0,backgroundColor:'#1DA1F2', borderBottomRightRadius: 0 }}
                                // source={require('../assests/twitter.png')}
                            >
                                <View style={{ height: 60, flexDirection: 'row', margin: 10, alignItems: 'center' }}>
                                    <TouchableOpacity
                                    // onPress={() => { this.props.navigation.goBack() }}
                                    >
                                        <Image source={require('../assests/twittermenu.png')} style={[{ width: 25, height: 25,tintColor:'#fff', marginLeft: 10, alignSelf: 'center' },{transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]}]}></Image>
                                    </TouchableOpacity>

                                    <Text style={{ fontSize: 22, color: 'white', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>Profile</Text>
                                    {/* <TouchableOpacity style={{ height: 40, marginTop: 7, width: 85, position: 'absolute', right: 10, top: 10, right: 10, marginRight: 10 }} onPress={() => { this.props.navigation.navigate('ProfileScreenAr') }}>

                                        <View style={styles.changeLanguageViewStyletoAr}>
                                            <Text style={{ fontSize: 13, fontFamily: 'Cairo-Bold', alignSelf: 'center', marginRight: 5 }}>عربى</Text>
                                            <View style={{ height: 30, width: 30, left: 15, right: 15, alignItems: 'center', borderRadius: 40, backgroundColor: 'white', marginLeft: 0 }}>
                                                <Image source={images.ksaflag} style={{ height: 30, width: 30, borderRadius: 40, left: 10, right: 10, marginLeft: -30 }}></Image>
                                            </View>
                                        </View>
                                    </TouchableOpacity> */}
                                </View>

                            </ImageBackground>

                        </View>
                        <View style={{ flexDirection: 'column', height: 30, backgroundColor: '#F4F5F6', borderRadius: 2 }}>

                            {/* <View style={{ width: '80%', alignSelf: 'center', height: 60, borderRadius: 5, padding: 10, marginTop: -30, backgroundColor: 'white' }}>
                                <Text style={{ fontSize: 17, fontWeight: 'bold' }}>Riyadh National hospital</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={location} style={{ width: 10, height: 13, alignSelf: 'center' }}></Image>
                                    <Text style={{ fontSize: 10, marginLeft: 10 }}>Riyadh, Saudi Arabia 12812</Text>

                                </View>
                            </View> */}

                            <View style={{ flexDirection: 'column', height: 30, backgroundColor: '#white', borderRadius: 2 }}>
                                <View style={{
                                    // width: '80%', alignSelf: 'center', height: 80, borderRadius: 5, padding: 10, marginTop: -50, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', 
                                    // shadowOffset: { height: 2, width: 2},
                                    // shadowColor:'#d3d3d3',
                                    // shadowOpacity: 2,
                                    // shadowRadius: 5,
                                    // elevation: 5
                                }}>
                                    <View style={{ flexDirection: 'row', flex: 1 }}>
                                        <Image source={require('./../assests/male.png')} style={{ marginLeft: 20, width: 100, height: 100, borderRadius: 100 / 2, marginTop: -50, borderWidth: 4, borderColor: 'white' }}></Image>
                                        {/* <Text style={{ marginLeft: 20, marginTop: -80, }}>Hello</Text> */}
                                    </View>
                                    <View style={{padding:20,marginTop:35}}>
                                    <Text style={{ fontFamily:Fonts.QuickSand_Bold,fontSize:20}}>{global.userData.name}</Text>
                                    <Text style={{ fontFamily:Fonts.QuickSand,fontSize:14,color:'grey',bottom:6 }}>@{global.userData.userName}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 0.7,backgroundColor:''}}>


                            <View style={{ flexDirection: 'column', marginTop: 90, }}>

                                { global.userData.userID ?
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={styles.formInput}
                                    value= {global.userData.userID}
                                    onBlur={this.onBlur}
                                    editable={false}
                                >USER_ID</FloatingLabel>
                                :
                                null
                                }

                                { global.userData.userName ?

                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={styles.formInput}
                                    value= {global.userData.userName}
                                    onBlur={this.onBlur}
                                    editable={false}
                                >{'USERNAME'}</FloatingLabel>
                                :
                                null
                                }

                                { global.userData.email ?
                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={styles.formInput}
                                    value= {global.userData.email}
                                    onBlur={this.onBlur}
                                    editable={false}
                                >EMAIL</FloatingLabel>
                                :
                                  null
                                }

                                { global.userData.name ?

                                <FloatingLabel
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.input}
                                    style={styles.formInput}
                                    value= {global.userData.name}
                                    onBlur={this.onBlur}
                                    editable={false}
                                >{'NAME'}</FloatingLabel>
                                :
                                null
                                }

                            <Text style={{width:'100%',textDecorationLine: 'line-through',height:1,backgroundColor:'#D7DDF3',}}></Text>

                            </View>

                        </View>
                        {/* <TouchableOpacity
                            style={{ height: 50, borderRadius: 31,marginBottom:30}}
                            onPress={() => { this.props.navigation.navigate('LoginScreen',{  }) }}
                        >
                            <View style={{ flexDrection: 'row', height: 50,left:'5%', width: '90%',backgroundColor:'#1DA1F2', borderRadius: 31,borderWidth:1,borderColor:'#9DB2F8', alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ flexDirection: 'row', fontSize: 16,padding: 10, color: '#FFF', fontFamily: Fonts.QuickSand_Bold }}>LOGOUT</Text>
                            </View>
                        </TouchableOpacity> */}
                    </View>

                </View>
                </SafeAreaView>

            // </ScrollView >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 65,
        backgroundColor: 'white',
    },
    labelInput: {
        color: 'gray',
        fontFamily: Fonts.QuickSand
    },
    formInput: {
        borderBottomWidth: 0.5,
        marginLeft: 10,
        borderColor: 'gray',
        fontFamily: Fonts.QuickSand
    },
    input: {
        borderWidth: 0,
        color: '#1DA1F2',
        height: 42,
        fontFamily: Fonts.QuickSand,
        fontSize:16
    },
    changeLanguageViewStyletoAr: {
        borderRadius: 20,
        height: 40,
        borderWidth: 1,
        borderColor: 'black',
        width: 100,
        marginTop:5,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    popupCardDiscount:
    {
    //   height:260,
      width:SCREEN_WIDTH-20,
    //   borderWidth: 0,
      borderRadius:20,
      backgroundColor:'#fff'
    },
    placeholder1:
    {
      width: SCREEN_WIDTH-40,
    //   backgroundColor: 'red',
      height: 50,
      borderRadius:10,
      fontFamily: Fonts.QuickSand_Semi_Bold
    },
    Button1:
    {
        marginTop:12,
        width:'80%',
        alignSelf:'center',
      marginBottom: 5,
      borderRadius:10,
    //   fontFamily: Fonts.QuickSand_Bold,
      backgroundColor: '#016FA6',
    },


});