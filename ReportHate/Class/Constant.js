import React from 'react'
import {View,
Text ,
Image,
Modal,
Dimensions,
StyleSheet,
TouchableOpacity,
Alert,
I18nManager,
ActivityIndicator,Platform,} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

const { width,height } = Dimensions.get('window');
// import moment from "moment";

import Datastore from 'react-native-local-mongodb'

const userDb = new Datastore({ filename: 'userData', autoload: true });
const StreamDb = new Datastore({ filename: 'streamData', autoload: true });
const UserStreamsDb = new Datastore({ filename: 'UserstreamData', autoload: true });

class Constant
{

  userDb()
  {
    return userDb
  }

  StreamDb()
  {
    return StreamDb
  }

  UserStreamsDb()
  {
    return UserStreamsDb
  }

  _storeData = async (key,value) => {
    try {
      await AsyncStorage.setItem(
        key,
        value
      );
    } catch (error) {
    // Error saving data
    }
  };

  async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    }
    catch(exception) {
      return false;
    }
  }

  showLoader(text)
  {
    return(
      <Modal
        animationType="slide"
        transparent={true}
        visible={true}
        onRequestClose={() => {
      }}>
        <View
          style = {{height : '100%',width : '100%', elevation:5, alignItems : 'center'
          ,justifyContent  :'center',position : 'absolute',zIndex:2,backgroundColor:'rgba(0,0,0,0)'}}
        >
          {/* <View style = {{padding:30,backgroundColor:this.appLightColor(),elevation:4,borderRadius:8}}> */}
          <ActivityIndicator size="large" color= {'#45BF5'} />
          <Text style = {[{color: 'pink',fontSize:15}]}>{text}</Text> 
          {/* </View> */}
        </View>
      </Modal>
    )
  }

  Price(price)
  {
    console.log(price);
    if(price != null)
    {
    var data = parseFloat(price.toString()).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    // console.log(price.replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    return I18nManager.isRTL ? `${data} ${language.SAR}` : `${language.SAR} ${data}`
    }
    return I18nManager.isRTL ? `${0} ${language.SAR}` : `${language.SAR} ${0}`

  }

  round(num, decimalPlaces) {
    var p = Math.pow(10, 0);
    var m = (num * p) * (1 + Number.EPSILON);
    return Math.round(m) / p;
  }

  PriceWithInt(price)
  {
    console.log(price);
    if(price != null)
    {
    var data = parseFloat(price.toString()).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    // console.log(price.replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    return I18nManager.isRTL ? `${data} ${language.SAR}` : `${language.SAR} ${data}`
    }
    return I18nManager.isRTL ? `${0} ${language.SAR}` : `${language.SAR} ${0}`

  }

}

module.exports = new Constant()