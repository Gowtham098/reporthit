// import { Fonts } from './../Class/utils/fonts'

import { Text, Image,I18nManager,View, StyleSheet,  FlatList, StatusBar, TouchableOpacity, ActivityIndicator } from 'react-native'

export const Fonts = {
    QuickSand : I18nManager.isRTL ? 'Cairo-Regular' : 'FreeSans',
    QuickSand_Bold : I18nManager.isRTL ? 'Cairo-Bold' : 'FreeSansBold',
    // QuickSand_Semi_Bold : 'Quicksand-SemiBold',
    QuickSand_Semi_Bold : I18nManager.isRTL ? 'Cairo-SemiBold' : 'Quicksand-SemiBold',
}