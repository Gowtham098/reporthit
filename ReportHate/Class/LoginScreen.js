import React, { Component } from "react"
import {
  AppRegistry,
  Button,
  Dimensions,
  StyleSheet,
  Text,
  View,
  Alert,
  Image,
  StatusBar,
  NativeModules,
  TouchableOpacity } from "react-native"
  import LinearGradient from 'react-native-linear-gradient'
  import { Fonts } from './utils/fonts'
  import animate from 'react-native-animate';
  import logo from '../assests/twitter.png'
  import Constant from './Constant'

  import Realm from 'realm';
  let realm;

  let SCREEN_HEIGHT = Dimensions.get('window').height;
  let SCREEN_WIDTH = Dimensions.get('window').width;

const { RNTwitterSignIn } = NativeModules

const Constants = {
  //Dev Parse keys
  TWITTER_COMSUMER_KEY: "rD7M0GceWorCs7uWdaxsQ8ebo",
  TWITTER_CONSUMER_SECRET: "PpIlugzL3cl76WVg8Tk7X0yxdKQcqgr3CWp1KddrBTv7m7Rorl"
}

export default class TwitterButton extends Component {
  state = {
    isLoggedIn: false,

    UserData : {},

    UserDetail : {},
   }

   constructor(props) {
    super(props);
    this.state = {
      GetStreamssArray : [],
      UserSubscriptionsArray : [],
    };
    realm = new Realm({ path: 'UserDatabase.realm' });
  }

  UserSubscriptions()
    {
        // console.log('https://6064-40280.el-alt.com/api/UserSubscriptions')

        let params = JSON.stringify({
            // "userId": global.userRegisteredID
            // "userId": 25
          })
          // alert(global.userRegisteredID)

        fetch('https://6064-40280.el-alt.com/api/UserSubscriptions', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            if (responseJson.responseData != null)
            {
              this.insertStreams(responseJson.responseData)
            }
            else
            {
            }
            // return responseJson.responseData

            // alert(JSON.stringify(responseJson.responseData))
          this.setState({ UserSubscriptionsArray : responseJson.responseData, isLoading: false })
          // alert(JSON.stringify(this.state.UserSubscriptionsArray))

        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

  getStreamsFromApi()
    {
      return fetch('https://6064-40280.el-alt.com/api/GetStreams')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({isLoading:false})

        if (responseJson.responseCode == "Success")
        {
          console.log(JSON.stringify(responseJson.responseData))

          this.setState({ GetStreamssArray : responseJson.responseData, isLoading: false })
          // alert(JSON.stringify(this.state.GetStreamssArray))

          // realm.write(() => {
  
          //   // var ID =
          //   //   realm.objects('AllStreamsData').sorted('user_id', true).length > 0
          //   //     ? realm.objects('AllStreamsData').sorted('user_id', true)[0]
          //   //         .user_id + 1
          //   //     : 1;

          //   var data = JSON.stringify(responseJson.responseData)
          //   // alert(data)
    
          //   realm.create('AllStreamsData', {
          //     data
          //   });

          // });

          global.topicId = responseJson.responseData[0].id
          // this.GetTweetsAPI()
          // alert(JSON.stringify(this.state.GetStreamssArray))
        }
        else if(responseJson.success == "false")
        {
          console.log(responseJson.responseMessage)
        }
        else
        {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        console.log(error);
      });
    }

    insertStreams(streamsObj)
    {
      streamsObj.map((item)=>{
        // alert(JSON.stringify(item))
        this.checkStreams(item)
      })
    }

    checkStreams(item)
    {
      // alert(item.id)
      Constant.StreamDb().find({id: item.id},function (err,docs)
      {
        // alert(JSON.stringify(docs))
        if(docs.length>0)
        {
          // alert(docs.length)
          Constant.StreamDb().update({id: item.id }, item, {}, function(err,numReplaced)
          {
            // alert(numReplaced)
          });
        }
        else
        {
          Constant.StreamDb().insert((item), function(err,newDocs)
          {
          });
        }
      });
    }


  componentDidMount()
  {
    // this.getStreamsFromApi()
    // this.UserSubscriptions()
  }

  _twitterSignIn = () => {
    RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
    RNTwitterSignIn.logIn()
      .then(loginData => {
        console.log(loginData)

        this.setState({UserData: loginData})
        global.userData = loginData
        // alert(JSON.stringify(global.userData))
        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          this.setState({
            isLoggedIn: true,
          })
        }
      })
      .catch(error => {
        console.log(error)
      }
    )
  }

  navigate()
  {
    let userData = [global.userData]
    // alert(JSON.stringify(userData))
    this.insertUserData(userData)
    this.props.navigation.navigate('Home', {  }) 
  }

  insertUserData(userobj)
  {
    userobj.map((item)=>{
      // alert(JSON.stringify(item))
      this.checkUser([item])
    })
  }

  checkUser(item)
  {
    Constant.userDb().find({userID : item.userID}, function (err, docs)
    {
      // alert(JSON.stringify(docs))

      if(docs.length>0)
      {
        Constant.userDb().update({ userID : item.userID }, item, {}, function (err, numReplaced) {
          // alert('number emp',numReplaced)
        });
      }
      else
      {
        Constant.userDb().insert((item),function(err,newDocs){
          // alert(JSON.stringify(newDocs))
        });
      }
    });
  }

  handleLogout = () => {
    console.log("logout")
    RNTwitterSignIn.logOut()
    this.setState({
      isLoggedIn: false
    })
  }

  render() {
    const { isLoggedIn } = this.state
    return (
      <View style={this.props.style}>
        <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />
        {isLoggedIn ? 

          this.navigate()

          // this.props.navigation.navigate('Home', { StreamsData : this.state.GetStreamssArray, UserStreams : this.state.UserSubscriptionsArray  }) 

        :
        // <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height + 100, alignItems: 'center', justifyContent: 'center', tintColor: 'white', paddingRight: 20,backgroundColor:'#1DA1F2' }} >

        //   <View style={{height:SCREEN_HEIGHT,justifyContent:'center'}}>
        //       <View style={{ width: Dimensions.get('window').width,height: Dimensions.get('window').height, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
        //         <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />
        //         <View style={{
        //             justifyContent: 'center',
        //             alignItems: 'center',
        //         }}>


        //             <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height + 200, alignItems: 'center', justifyContent: 'center', tintColor: 'white', paddingRight: 20,backgroundColor:'#1DA1F2' }} >
                   
        //            <View style = {{alignItems:'center',marginLeft:20,width:'100%',justifyContent:'center',flex:1,marginBottom:40}}>
        //             <View style={{
        //                 height: 200,
        //                 marginTop:290,
        //                 width:'100%',
        //                 justifyContent: 'center', 
        //                 alignItems: 'center',
        //             }}>

        //                 <Image source={logo} style={{ width: 150,marginLeft:20, height: 150,resizeMode:'contain', justifyContent: 'center', alignItems: 'center' }}></Image>
                  
        //             </View>

        //             <View style={styles.bottomContainer}>
        //                 <View style={{ flexDirection: "row", alignItems: 'center', justifyContent: 'center' }}>
        //                     <Text style={{color: 'white',fontSize: 22,fontFamily: Fonts.QuickSand_Bold,marginLeft:5}}>ReportHate</Text>
        //                 </View>

        //                 <View style={{ flexDirection: "column", marginTop: 10,position:'absolute',bottom:0,width:'100%' }}>
        //                   <TouchableOpacity style={{ height: 50, borderRadius: 31,}} onPress={this._twitterSignIn}>
        //                     <View style={{ flexDrection: 'row', height: 50,left:'5%', width: '95%',backgroundColor:'#fff', borderRadius: 31,borderWidth:1,borderColor:'#9DB2F8', alignItems: 'center', justifyContent: 'center' }}>
        //                         <Text style={{ flexDirection: 'row', fontSize: 16,padding: 10, color: '#1DA1F2', fontFamily: Fonts.QuickSand_Bold }}>LOGIN WITH TWITTER</Text>
        //                     </View>
        //                   </TouchableOpacity>
        //                 </View>
        //             </View>
        //             </View>
        //         </View>
                    
        //         </View>
        //     </View>

            

        //   </View>
        //   </View>

        <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height + 100, alignItems: 'center', justifyContent: 'center', paddingRight: 20,backgroundColor:'#1DA1F2'}}>
          <View style={{ alignItems:'center',justifyContent:'center'  }}>
            <Image source={logo} style={{ width: 150,marginLeft:20, height: 150,resizeMode:'contain', justifyContent: 'center', alignItems: 'center' }}></Image>
            <Text style={{color: 'white',fontSize: 22,fontFamily: Fonts.QuickSand_Bold,marginLeft:5}}>ReportHate</Text>
          </View>
          <View style={{backgroundColor:'',width: Dimensions.get('window').width, height: 50,position:'absolute',bottom:100,left:0,right:0}}>
            <TouchableOpacity style={{ height: 50, borderRadius: 31,}} onPress={this._twitterSignIn}>
              <View style={{ flexDrection: 'row', height: 50,left:'2.5%', width: '95%',backgroundColor:'#fff', borderRadius: 31,borderWidth:1,borderColor:'#9DB2F8', alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ flexDirection: 'row', fontSize: 16,padding: 10, color: '#1DA1F2', fontFamily: Fonts.QuickSand_Bold }}>LOGIN WITH TWITTER</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        }
        </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#1b95e0',
    color: 'white',
    width: 200,
    height: 50
  },
  bottomContainer:
  {
    width:'100%',
    // backgroundColor:'pink',
    height:'35%'
  }
})