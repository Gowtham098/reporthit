import React, { Component } from 'react'
import { Text,I18nManager, Image, View, StyleSheet, Dimensions, Animated,ImageBackground,StatusBar } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

import logo from '../assests/twitter.png'
import animate from 'react-native-animate';
import LinearGradient from 'react-native-linear-gradient'
import { Fonts } from './utils/fonts'
import { NavigationActions, StackActions } from 'react-navigation';

import Constant from './Constant'

import Realm from 'realm';
let realm;

export default class SplashScreen extends React.Component {

    onFadeIn = () => animate(
        this.state.opacity,
        1,
        3000,
        'ease-in-out',
        1000,
        false,
        () => console.log('we are now faded in!'),
    );

    constructor(props) {
        super(props);

        this.state = {
            opacity: animate(1),
            THREE_SECONDS: 3000,
            userArray : [],
        }
      }

    // constructor(props) {
    //     super(props);

    //     realm = new Realm({
    //         path: 'UserDatabase.realm',
    //         schema: [
    //           {
    //             name: 'user_details',
    //             properties: {
    //               user_id: { type: 'int', default: 0 },
    //               user_twitter_id : 'string',
    //               user_name: 'string',
    //               user_handle: 'string',
    //               user_email: 'string',
    //               authToken : 'string',
    //               authTokenSecret : 'string'
    //             },
    //           },
    //         ],
    //       });

    //     this.state = {
    //         opacity: animate(1),
    //         THREE_SECONDS: 3000
    //     }

    // }

    getUserData()
    {
      const _this=this
       Constant.userDb().find({}, function (err, docs)
       {
        // alert(JSON.stringify(docs))
         _this.setState({userArray : docs})
        //  alert(JSON.stringify(_this.state.userArray))
        //  console.log("cashout",docs)
      });
    }

    componentDidMount() {
        setTimeout(() => {

          const _this = this
        AsyncStorage.getItem('userData').then((value) => {
        
        if(value != null)
        {
        let userData = JSON.parse(value)
        global.userData = userData

        // global.userRegisteredID = userRegisteredID
        // global.streamIndex = streamIndex
        // global.topicId = topicId
        // global.totalTweetsCount = totalTweetsCount
        // global.tweetId = tweetId
        // global.count = count

        // AsyncStorage.removeItem('userData')
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
        _this.props.navigation.dispatch(resetAction);

        }
        else{

            const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
        });
        _this.props.navigation.dispatch(resetAction);
        }
    })
           
        }, this.state.THREE_SECONDS)

        this.getUserData()
    }
    render() {

        return (

            <View style={{ width: Dimensions.get('window').width,height: Dimensions.get('window').height, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <StatusBar backgroundColor="#1DA1F2" barStyle="light-content" />
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                 {/* <StatusBar hidden={true} /> */}


                    <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height + 200, alignItems: 'center', justifyContent: 'center', tintColor: 'white', paddingRight: 20,backgroundColor:'#1DA1F2' }} >
                   
                   <View style = {{alignItems:'center',justifyContent:'center',flex:1,marginBottom:40}}>
                    <View style={{
                        height: 200,
                      marginTop:100,
                        justifyContent: 'center', 
                        alignItems: 'center'
                    }}>

                        <Image source={logo} style={{ width: 150,marginLeft:20, height: 150,resizeMode:'contain', justifyContent: 'center', alignItems: 'center' }}></Image>
                        <View style={{ flexDirection: 'row' }}>

                            {/* <Text style={{color:'#f88b24'}}>ice</Text> */}
                        </View>
                    </View>

                    <View style={styles.bottomContainer}>
                        <View style={{ flexDirection: "row", alignItems: 'center', justifyContent: 'center' }}>
                            {/* <Image source={shield} style={{ width: 20, height: 25 }}></Image> */}
                            {/* <Text style={{color: 'white',fontFamily: Fonts.QuickSand_Bold,marginLeft:5}}>{language.titleS}</Text> */}
                            <Text style={{color: 'white',fontSize: 22,fontFamily: Fonts.QuickSand_Bold,marginLeft:5}}>ReportHate</Text>
                        </View>

                        <View style={{ flexDirection: "column", marginTop: 10 }}>
                            {/* <Text style={{ color: 'white',padding:10,textAlign:'center',fontSize:12, lineHeight:20,fontFamily: Fonts.QuickSand_Bold }}>{language.stringSTwo}</Text>     */}
                            {/* <Text style={{ color: 'white',padding:10,textAlign:'center',fontSize:12, lineHeight:20,fontFamily: Fonts.QuickSand_Bold }}>{language.stringSTwo}</Text>     */}
                        </View>
                    </View>
                    </View>
</View>
                    
                </View>
                {/* <Image style={{ width: 90, height: 90 }} source={bgImage}></Image> */}
            </View>

        );
    }
}
const styles = StyleSheet.create({
    Container: {
        flex: 1
    },
    bottomContainer: {
      
      

    },
})
