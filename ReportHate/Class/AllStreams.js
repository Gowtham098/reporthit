import React, { Component } from "react"
import DeviceInfo from 'react-native-device-info';
import {
  AppRegistry,
  Button,
  Dimensions,
  StyleSheet,
  Text,
  View,
  FlatList,
  NativeModules,
  Alert,
  TouchableOpacity } from "react-native"
import { Fonts } from "../Class/utils/fonts";
import Constant from './Constant'

  let SCREEN_HEIGHT = Dimensions.get('window').height;
  let SCREEN_WIDTH = Dimensions.get('window').width;

  const DATA = [
    {
      "id": 1,
      "topicName": "#VMovie"
      },
      {
      "id": 2,
      "topicName": "#SushantSinghRajput"
      },
      {
      "id": 3,
      "topicName": "#Corona Virus 2019"
      },
      {
      "id": 4,
      "topicName": "#Supreme Court"
      }

];

  export default class Streams extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        GetStreamssArray : [],
        SubscriptionsArray : [],
        UserStreamsArray : [],
        isSubscribe: false,
        isLoading : this.props.isLoading,

        // GetStreamsData : this.props.GetStreamsData,
      };
      this.getStreamsFromApi()
    }

    componentDidMount()
    {
      const { navigation } = this.props;

      this.focusListener = navigation.addListener('willFocus', () => {
      })
      if(this.props.navigation.state.params)
      {
      }
    //   alert(JSON.stringify(global.userRegisteredID))
      this.getUserStreams()
      // console.log(DeviceInfo.getUniqueId())
      // this.dataLoad()

    }

    getUserStreams()
    {
      const _this=this
       Constant.UserStreamsDb().find({}, function (err, docs)
       {
        // alert(JSON.stringify(docs))
         _this.setState({UserStreamsArray : docs})
        //  console.log("cashout",docs)
      });  
    }

    dataLoad()
    {
        let arr = this.state.GetStreamsData.map(( item, index)=> {
            item.isSubscribe = false
            return {...item}
          })
        //   alert(JSON.stringify(arr))
          this.setState({ GetStreamsData : arr });
    }

    getStreamsFromApi()
    {
      this.setState({ isLoading : true})
      // return fetch('https://6064-40280.el-alt.com/api/GetStreams')
      return fetch('http://6064-40280.el-alt.com/api/GetAllTopics/UserId/'+global.userRegisteredID)
      // http://6064-40280.el-alt.com/api/GetAllTopics/UserId/33
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({isLoading:false})

        if (responseJson.responseCode == "Success")
        {
          // alert(JSON.stringify(responseJson.responseData))
          this.setState({ GetStreamssArray : responseJson.responseData, isLoading: false })
        }
        else if(responseJson.success == "false")
        {
          console.log(responseJson.responseMessage)
          this.setState({ GetStreamssArray : [], isLoading: false })
        }
        else
        {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        console.log(error);
      });
    }

    SubscriptionsAPI()
    {
      // console.log(Endpoints.DEV_URL+'/api/SubscribeStream);
      // return fetch(Endpoints.DEV_URL+'/api/SubscribeStream?nid='+this.state.National_ID+'&oldpassword='+this.state.oldPassword+'&newpassword='+this.state.newPassword)

      fetch('https://6064-40280.el-alt.com/api/SubscribeStream', {

        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "userId": global.userRegisteredID,
          "topicId": global.streamIndex,
        })

      })

      .then((response) => response.json())
      .then((responseJson) => {

        if (responseJson.responseCode == "Success")
        {
          alert(JSON.stringify('Subscribed'))
        //   this.setState({ SubscriptionsArray : responseJson.responseData, isLoading: false })
        }
        else if(responseJson.success == "false")
        {
          console.log(responseJson.responseMessage)
        }
        else
        {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    render_FlatList_EmptyView = () => {
 
      var emptyView = (
   
      <View style={styles.emptyView}>
   
        <Text style={styles.textStyle}>No data found</Text>
   
      </View>
   
      );
   
      return emptyView ;
   
  };

    tappedOnDetails =(index) =>
    {
      Alert.alert('','Do you want to subscribe this stream?',
        [
          {text: 'CANCEL', onPress: () => console.log('Cancel Pressed')},
          {text: 'SUBSCRIBE', onPress:() => this.deleteAlert(index)},
        ],
        { cancelable: true } 
      )
    }

    deleteAlert = (item,ind) =>
    {
        let arr = this.state.GetStreamssArray.map(( item, index)=> {
            if( ind == index )
            {
              // alert(JSON.stringify(item))
              if(!item.isSubscribe)
              {
                item.isSubscribe = !item.isSubscribe
              }
            }
            return {...item}
          })
        //   alert(JSON.stringify(arr))
          this.setState({ GetStreamssArray : arr });

        var array = this.state.GetStreamsData
        // alert(JSON.stringify(item.id))
        global.streamIndex = item.id
        this.setState({isSubscribe: !this.state.isSubscribe});

        this.SubscriptionsAPI()
        // array.splice(index,1);

        //   alert(JSON.stringify(DATA))
        this.setState({ GetStreamsData: array});
    }

    render() {
        let GetStreamsData = this.state.GetStreamsData
        // alert(JSON.stringify(GetStreamsData))
        
        // let tempList = this.state.GetStreamssArray.map(item => item.id);

        // let result = this.state.UserStreamsArray.filter(item => (tempList.includes(item.id)))
        // alert(JSON.stringify(result))
        
      return(
        <View style = {{flex : 1}}>
          {this.state.isLoading ? Constant.showLoader() : null}
          <FlatList
            data={ this.state.GetStreamssArray }
            extraData={this.state}
            // data={this.state.dataSource}

            renderItem={
              ({ item,index }) => {
                return(
              <TouchableOpacity
              onPress={() => { this.props.navigation.navigate('TweetsById',{ topicDetails : item,topicId : item.id}) }}
              activeOpacity={0.8}
              style={{
                  flex: 1, flexDirection: 'row', borderRadius: 15, backgroundColor: 'white', padding: 10, margin: 10, shadowOffset: { height: 2, width: 2 },
                  shadowOpacity: 2,
                  shadowColor:'#d3d3d3',
                  shadowRadius: 5,
                  elevation: 5,
              }}>
                <View style={{flex:1,paddingLeft:8,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                    {/* <Text style={{ fontSize:16,width:'60%'}}>Hello :</Text> */}
                    <View style = {{width: '68%',}}>
                        <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>{item.topicName}</Text>
                    </View>
                    <TouchableOpacity
                        // onPress={() => { alert('1')}}
                        onPress={()=> this.deleteAlert(item,index)}
                        style={{height:40,width:'32%',alignItems:'center',justifyContent:'center'}}
                    >
                        { item.isSubscribed || item.isSubscribe ?
                        <View style={{height:40,width:'100%',borderRadius:20,backgroundColor:'#8391C5',alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'#fff',fontSize:16,fontFamily: Fonts.QuickSand_Semi_Bold}}>Subscribed</Text>
                        </View>
                        :
                        <View style={{height:40,width:'100%',borderRadius:20,backgroundColor:'#1DA1F2',alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'#fff',fontSize:16,fontFamily: Fonts.QuickSand_Semi_Bold}}>Subscribe</Text>
                        </View>
                    }
                    </TouchableOpacity>
                </View>
              </TouchableOpacity>
                )}

            }
            keyExtractor={item => item.id}
            ListEmptyComponent={this.props.isLoading ? null : this.render_FlatList_EmptyView()}
          />
        </View>
      )
    }
  }

  const styles = StyleSheet.create({
    emptyView:
    {
        flex:1,
        height:SCREEN_HEIGHT,
        alignItems:'center',
        justifyContent:'center',
    },
    textStyle:
    {
        textAlign: 'left', 
        color: 'black',
        fontFamily: Fonts.QuickSand_Bold,
        fontSize: 16
    }
})