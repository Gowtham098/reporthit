// import { View } from 'native-base';
import {
    AppRegistry,
    Button,
    Dimensions,
    View,
    StyleSheet,
    Text,
    Linking,
    Modal,
    FlatList,
    Image,
    ImageBackground,
    StatusBar,
    TouchableOpacity } from "react-native"
    import { Fonts } from "../Class/utils/fonts";
import React, { Component } from 'react';
import Dots from 'react-native-dots-pagination';
import Constant from "./Constant";
import { TextInput, Card } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient'
 
let SCREEN_HEIGHT = Dimensions.get('window').height;

let SCREEN_WIDTH = Dimensions.get('window').width;
 
export default class Example extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 0,
      reportArray : [],
      isLoading: false,
      totalTweetsCount : global.totalTweetsCount,

      isDecrement : false,
      isIncrement : false,

      pages: 0,
      currentPage: 0,
      pageNumber : 1,
    }
  }

  componentDidMount()
  {
      if (this.props.navigation.state.params)
        {

        }
        this.reportAPI()
  }

  reportAPI()
    {
        // console.log('https://6064-40280.el-alt.com/api/GetTweetsByTopicId')
        this.setState({ isLoading: true })

        let params = JSON.stringify({
            "page": this.state.pageNumber, 
            "perpage": 10,  
            "userId": global.userRegisteredID,
          })
        //   alert( params )

        fetch('https://6064-40280.el-alt.com/api/GetUserReportedTweets', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson.responseData))

          this.setState({ reportArray : responseJson.responseData,totalTweetsCount : responseJson.totalTweetsCount, isLoading: false })
          global.totalTweetsCount = responseJson.totalTweetsCount
        //   alert(global.totalTweetsCount)

        } else if(responseJson.success == "false") {

            this.setState({isLoading:false})
            // alert(responseJson.responseMessage)
            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
          this.setState({isLoading:false,isIncrement: true,isDecrement: false})
          global.totalTweetsCount = 0
          alert(JSON.stringify(responseJson.responseData))
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    incrementCount()
    {
      var count = global.totalTweetsCount
      count = count / 10;
      var data = Constant.round(count)
    //   alert(data)
      if(data >  this.state.pageNumber)
      {
        this.setState ({
          pageNumber: this.state.pageNumber + 1,
          isDecrement : true
        });
      }
      else{
        this.setState ({
          isIncrement : true,
        });
      }
    this.reportAPI()
      // alert(this.state.pageNumber)
    }

    decrementCount()
    {
      var count = global.totalTweetsCount
      count = count / 10;
      var data = Constant.round(count)
      // alert(data)
      if(data <= this.state.pageNumber)
      {
        // alert(this.state.pageNumber)
        this.setState({ pageNumber : this.state.pageNumber + 1, isIncrement : false})
        // alert(this.state.pageNumber)
    }
    else{
      this.setState ({
        isDecrement : false,
      });
    }
      // alert(this.state.pageNumber)
      this.reportAPI()
    }

    touchtohide = () => {
      this.setState({ isSort: false })
    }
  
    touchtodisplay = (item) => {
      // alert(JSON.stringify(item.tweetId))
      global.tweetId = item.tweetId
      this.setState({ isSort: true })
    }

    reportPage()
    {
      this.setState({isSort:false})
       this.props.navigation.navigate('ReportPage',{ }) 
    }

    PageCount()
    {
      let count = this.state.totalTweetsCount
      // alert(count)
      count = count / 5
      // alert(parseFloat(count))
      // var data = Math.round(count)
      // alert(count)
      if( count >= 0 && count <= 1 )
      {
        count = 1
      }
      else if( count > 1 && count <= 2 )
      {
        count = 2
      }
      global.count = count
      // alert(global.count)
      return count
    }

    render_FlatList_EmptyView = () => {
 
        var emptyView = (
     
        <View style={styles.emptyView}>

          <Text style={styles.textStyle}>No Data Found</Text>
     
        </View>
     
        );
     
        return emptyView ;
     
    };
  
  render() {
    return (
        <View style={{flex:1}}>
          <View style={{height:'100%',width: SCREEN_WIDTH}}>
            <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />
            <View style={{ height: 80, borderBottomWidth : 1,borderBottomColor:'#E9E8E8', }}>
                <View style={{ flexDirection: 'row', height: 80,alignItems:'center',justifyContent:'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                      <TouchableOpacity
                    //   onPress={()=>{this.props.navigation.goBack(null)}}
                      >
                        <Image source={require('../assests/report.png')} style={[{ width: 25,resizeMode:'contain', height: 20, marginLeft: 10 },]}></Image>
                        {/* <ImageBackground source={images.back} style={[{ width: 25, height: 20,marginTop:2, marginLeft: 10 }]}></ImageBackground> */}
                      </TouchableOpacity>
                      <Text style={{ fontSize: 22, color: '#1DA1F2', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>Reported Tweets</Text>
                    </View>          
                  </View>
            </View>

            {/* <Dots length={1} active={this.state.active} /> */}
            {this.state.isLoading ? Constant.showLoader() : null}

            <FlatList
            data={ this.state.reportArray }
            extraData={this.state}
            contentContainerStyle={{ paddingBottom: 20}}
            // data={this.state.dataSource}

            renderItem={
              ({ item,index }) =>
              <View style={{flex:1,paddingLeft:8,margin:5,alignItems:'center',}}>
                                  <View style = {{flexDirection:'column',width:'100%',alignItems:'center'}}>
                                    <View style = {{width:'100%',flexDirection:'row',alignItems:'center'}}>
                                        <View style={{ height: 70, width: 70,   alignItems: 'center', borderRadius: 40, backgroundColor: 'white', marginLeft: 0 }}>
                                            <Image source={require('../assests/male.png')} style={{ height: 70, width: 70, borderRadius: 40, left: 15, right: 15, marginLeft: -30 }}></Image>
                                        </View>
                                        <View style={{flexDirection:'column',marginLeft:10}}>
                                        <Text style={{ fontSize:16,textAlign:'left',maxWidth:'100%',fontFamily: Fonts.QuickSand_Bold,marginRight:100 }}>{item.tweetId}</Text>
                                        <Text numberOfLines={1} style={{ fontSize:14,maxWidth:'100%',textAlign:'left',fontFamily: Fonts.QuickSand,color:'grey',marginRight:100,bottom:6 }}>{item.twitterHandle}</Text>
                                        </View>
                                        {/* <TouchableOpacity style= {{position:'absolute',right:0,height:20,width:20}} onPress={() => this.touchtodisplay(item)}>
                                          <Image source={images.downImage} style={{height:15,width:15,position:'absolute',right:10,tintColor:'grey'}} />
                                        </TouchableOpacity> */}
                                    </View>
                                    <View style = {{flexDirection:'row',width:'100%',alignItems:'center'}}>
                                        {/* <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>twitterProfileLink : {item.twitterProfileLink}</Text> */}
                                        <Text style={{ fontSize:14,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand,paddingTop:5,paddingBottom:5 }}>{item.tweet}</Text>
                                        {/* <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>tweetedOn : {item.tweetedOn}</Text>
                                        <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>rating : {item.rating}</Text> */}
                                    </View>
                                    {/* <Image source={images.div} style={{ height: 1.5, width: '100%', marginTop: 10 }}></Image> */}
                                    {/* <TouchableOpacity
                                    style={{height:40,width:'100%',alignItems:'center',justifyContent:'center',borderRadius:5,marginTop:5,backgroundColor:'#1DA1F2'}}
                                    onPress={() => { alert('Report') }}
                                    > */}
                                    <TouchableOpacity 
                                      style={{height:40,width:'100%',alignItems:'center',justifyContent:'center',borderRadius:5,marginTop:5,backgroundColor:'#1DA1F2'}}
                                    //   onPress={() => { 
                                    //     Linking.openURL('https://twitter.com/'+item.twitterHandle+'/status/'+item.tweetId);
                                    //   }}
                                    >
                                        <Text style={{color:'#fff',fontFamily: Fonts.QuickSand_Bold,}}>UnReport Tweet</Text>
                                    </TouchableOpacity>
                                    <Text style={{width:'100%',textDecorationLine: 'line-through',height:1,backgroundColor:'#D7DDF3',marginTop: 10}}></Text>
                                  </View>
                                </View>

            }
            keyExtractor={item => item.id}
            ListEmptyComponent={this.state.isLoading ? null : this.render_FlatList_EmptyView()}
          />
         <View style={{justifyContent:'space-between',flexDirection:'row',padding:10}}>
          <TouchableOpacity style={{justifyContent:'center'}} onPress={() => this.decrementCount()}>
            { this.state.isDecrement ?
              <Image source = {require('../assests/decrement.png')} style = {{height: 35,width: 35,resizeMode:'contain'}} /> 
            :
              null
            } 
          </TouchableOpacity>

          <TouchableOpacity style={{justifyContent:'center',}} onPress={() => this.incrementCount()}>
            { this.state.isIncrement ?
              null
            :
              <Image source = {require('../assests/increment.png')} style = {{height: 35,width: 35,resizeMode:'contain'}} />
            }
          </TouchableOpacity>
        </View>
          </View>
          <View style={{flexDirection:'row',width:'100%',position:'absolute',bottom:5,alignItems:'center',justifyContent:'center',}}>

            </View>

        </View>
    )
  }
}

const styles = {
emptyView:
    {
        flex:1,
        height:SCREEN_HEIGHT,
        alignItems:'center',
        justifyContent:'center',
    },
    textStyle:
    {
        textAlign: 'left', 
        color: 'black',
        fontFamily: Fonts.QuickSand_Bold,
        fontSize: 16
    },
}