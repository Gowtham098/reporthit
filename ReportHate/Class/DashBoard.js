import React, { Component } from 'react'
import { Text, Image,I18nManager, ImageBackground,TextInput,Linking, View, StyleSheet,BackHandler, ScrollView,Modal, Dimensions, Animated, FlatList, StatusBar, TouchableOpacity, ActivityIndicator } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { SafeAreaView } from 'react-native-safe-area-context';
import Constant from './Constant'
import { Fonts } from './utils/fonts'
import { SearchBar } from 'react-native-elements';
import { HeaderBackButton } from 'react-navigation-stack';

import Realm from 'realm';
let realm;

let SCREEN_HEIGHT = Dimensions.get('window').height;
let SCREEN_WIDTH = Dimensions.get('window').width;

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        name: 'Dental',
        icon: '',
        price: '1300 SAR /year',
        valid: '12-02-2021',
        tweetedOn: "sonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonusonu",
        tweetname : "sonusonusonu",
        tweeterhandle : "sonusonusonu",
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        name: 'Eye Care',
        icon: '',
        price: '534 SAR /year',
        valid: '12-02-2021',
        tweetedOn: 34566,
        tweetname : 'sonusonusonu',
        tweeterhandle : 'sonusonusonu',
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        name: 'Cardio',
        icon: '',
        price: '489 SAR /year',
        valid: '12-02-2021',
        tweetedOn: 34566,
        tweetname : 'sonusonusonu',
        tweeterhandle : 'sonusonusonu',

    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        name: 'Ortho',
        icon: '',
        price: '489 SAR /year',
        valid: '12-02-2021',
        tweetedOn: 34566,
        tweetname : 'sonusonusonu',
        tweeterhandle : 'sonusonusonu',
    },
];

export default class CoverageLimits extends React.Component {

    static navigationOptions = ({navigation}) => {
        const {state} = navigation;
        return{
          // title: window.helloComponent.titleStr ? window.helloComponent.titleStr : 'ADD USER',
          title: 'HOME',
          tintColor: '#ffffff',
          headerTintColor: '#ffffff',
          headerLeft: <HeaderBackButton tintColor = '#fff' onPress={() => navigation.goBack(null)} /> ,
          headerRight: (
            <View style ={{flexDirection : 'row'}}>
              
             <Text>right</Text>
            </View>
          ),
          headerStyle: 
          {
            backgroundColor: 'pink',
          },
          headerBackTitle: null,
        //   headerTitleStyle: {BaseStyle.headerStyle},
        };
      }

    constructor(props) {
        super(props);

        this.state = {
            BoolValue: '',
            code: 'CI',
            nationalID: '',
            yearOfBirth: '',
            insPolicyNo: '',
            dataSource: [],
            isLoading: false,
            isFilter : false,
            FilterArray : [],
            isSort: false,

            GetStreamssArray : [],
            UserSubscriptionsArray : [],
            tweetsArray : [],
            search: '',
            selectedItem:'null',
            isSearch : false,
            currentIndex:  0,
            searchData : [],
            userArray : [],

            pages: 0,
            currentPage: 0,
            pageNumber : 1,

            FlatListItems: [],
            isDecrement : false,
            isIncrement : false,
        };
        this.handleBackButton = this.handleBackButton.bind(this);

        this.getUserData()

        this.AddUser()
        this.UserSubscriptions()
        this.GetTweetsAPI()
    }

    handleBackButton() {
      return true;
    }

    AddUser()
    {
      // alert(global.userData.)
        // console.log(https://6064-40280.el-alt.com/api/AddUser);
        let params = JSON.stringify({

            "authToken": global.userData.authToken,
            "authTokenSecret": global.userData.authTokenSecret,
            "email": global.userData.email,
            "name": global.userData.name,
            "twitterUserID": global.userData.userID,
            "userName": global.userData.userName,
            

            // "authToken": this.state.FlatListItems[0].authToken,
            // "authTokenSecret": this.state.FlatListItems[0].authTokenSecret,
            // "email": this.state.FlatListItems[0].email,
            // "name": this.state.FlatListItems[0].name,
            // "twitterUserID": this.state.FlatListItems[0].userID,
            // "userName": this.state.FlatListItems[0].userName,
          })
        //   alert(params)

        fetch('https://6064-40280.el-alt.com/api/AddUser', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then(response => response.json())

      .then(responseJson => {
        //   alert("hlo")

          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson.responseData))
          this.setState({ isLoading: false })
          global.userRegisteredID = responseJson.responseData
          // alert(global.userRegisteredID)

        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }

      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    UserSubscriptions()
    {
        // console.log('https://6064-40280.el-alt.com/api/UserSubscriptions')
        this.setState({ isLoading : true})

        let params = JSON.stringify({
            "userId": global.userRegisteredID
            // "userId": 25
          })
          // alert(global.userRegisteredID)

        fetch('https://6064-40280.el-alt.com/api/UserSubscriptions', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson))
          this.setState({ UserSubscriptionsArray : responseJson.responseData, isLoading: false })
          // alert(JSON.stringify(this.state.UserSubscriptionsArray))


        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    incrementCount()
    {
      var count = global.totalTweetsCount
      count = count / 10;
      var data = Constant.round(count)
      // alert(data)
      if(data >  this.state.pageNumber)
      {
        this.setState ({
          pageNumber: this.state.pageNumber + 1,
          isDecrement : true
        });
      }
      else{
        this.setState ({
          isIncrement : true,
        });
      }
    this.GetTweetsAPI()
      // alert(this.state.pageNumber)
    }

    decrementCount()
    {
      var count = global.totalTweetsCount
      count = count / 10;
      var data = Constant.round(count)
      // alert(data)
      if(data <= this.state.pageNumber)
      {
        // alert(this.state.pageNumber)
        this.setState ({
          pageNumber: this.state.pageNumber - 1,
          isIncrement : false
        });
        // alert(this.state.pageNumber)
      
    }
    else{
      this.setState ({
        isDecrement : false,
      });
    }
      // alert(this.state.pageNumber)
      this.GetTweetsAPI()
    }

    GetTweetsAPI()
    {
        // console.log('https://6064-40280.el-alt.com/api/GetTweetsByTopicId')
        this.setState({ isLoading: true })

        let params = JSON.stringify({
            "page": this.state.pageNumber, 
            "perpage": 10,
            "query": "",
            // "topicId": 1,
            "topicId": global.topicId ,
          })
          // alert( this.state.pageNumber )

        fetch('https://6064-40280.el-alt.com/api/GetTweetsByTopicId', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson))

          this.setState({ tweetsArray : responseJson.responseData,totalTweetsCount : responseJson.totalTweetsCount, isLoading: false })

          global.totalTweetsCount = responseJson.totalTweetsCount
          // alert(global.totalTweetsCount)

        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }


    navigateScreen() {

        this.props.navigation.goBack(null)
        // if (this.state.BoolValue == true) {
        //     this.props.navigation.navigate('HomeScreen')
        // }
        // else {
        //     this.props.navigation.navigate('MedicalScreen')
        // }
    }

    reportPage()
    {
      this.setState({isSort:false})
       this.props.navigation.navigate('ReportPage',{ }) 
    }

    isSearch()
    {
        this.setState({ isSearch : !this.state.isSearch})
    }

    touchtohide = () => {
        this.setState({ isSort: false })
    }
    
    touchtodisplay = (itemData) => {
        // alert(JSON.stringify(itemData.id))

        // global.topicId = itemData.id
        // alert(global.topicId)

        // global.tweetId = item.tweetId
        this.setState({ isSort: true })
    }

    componentDidMount()
    {
      if (this.props.navigation.state.params) {
      
      }

      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    getUserData()
    {
      const _this=this
       Constant.userDb().find({}, function (err, docs)
       {
        // alert(JSON.stringify(docs))
         _this.setState({userArray : docs})
        //  alert(JSON.stringify(_this.state.userArray))
        //  console.log("cashout",docs)
      });  
    }

    isFilter(itemData,index)
    {
        // var data = this.state.dataSource
        // var newArray = data.filter(function(item) {
        //     return item.code == itemValue.code
        // });
        // console.log(JSON.stringify(newArray))
        // this.setState({FilterArray: newArray,isFilter:true})
        // alert(itemValue.id)

        // this.setState({ currentIndex: index })

        global.topicId = itemData.id
        // alert(global.topicId)

        this.GetTweetsAPI()
        // var data = this.state.tweetsArray
        // console.log(JSON.stringify(data))
        // this.setState({FilterArray: data,isFilter:true})
    }

    render_FlatList_EmptyView = () => {
 
        var emptyView = (
     
        <View style={styles.emptyView}>

          <Text style={styles.textStyle}>No Data Found</Text>
     
        </View>
     
        );
     
        return emptyView ;
     
    };

    topicName(itemName,index)
    {
      // console.log(index)
        var data = this.state.UserSubscriptionsArray
        var newArray = data.filter(function(item) {
            return item.index == index
        });
        // console.log(JSON.stringify(newArray))

        // global.topicId = itemName.id
        // console.log(global.topicId)
        var Name = itemName.topicName
        return Name
    }

    clear = () => {
      this.search.clear();
    };

    SearchFilterFunction(text)
    {
      this.setState({
        search:text,
      });
    }

    render() {

      let searchText = this.state.search

      var searchData = []
        if(this.state.tweetsArray.length > 0)
        {
            searchData = this.state.tweetsArray.filter(item => {
            // alert(JSON.stringify(item.tweet))
                const itemData =  `${item.tweet ? item.tweet.toUpperCase() : ''.toUpperCase()}` 
                const textData = searchText.toUpperCase();
                return itemData.includes(textData)
            });
        }

      const { currentIndex,search } = this.state
        return (
            <View style={{ flex: 1, flexDirection: 'column',width:SCREEN_WIDTH,height:SCREEN_HEIGHT }}>
              {this.state.isLoading ? Constant.showLoader() : null}
              <SafeAreaView edges={[ 'bottom',]} style={{flex:0,backgroundColor:'#016FA6'}}/>
              <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />
                <SafeAreaView edges={['right', 'bottom', 'left']} style={{ flex: 1,backgroundColor:'#fff'}}>
                    {/* <LinearGradient colors={['#0296CC', '#016FA6', '#004B82']} style={{flexDirection: 'column', backgroundColor: '#016FA6', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}> */}

                        <View style={{flexDirection: 'column', borderBottomLeftRadius: 20,paddingBottom:this.state.dataSource.length > 0 ? 0 : 10,  borderBottomRightRadius: 20 }}>
                            <View style={{ flexDirection: 'row', height: 60,borderBottomColor:'#C7CDE6',borderBottomWidth:1 }}>
                                <View style={{ flex: 1, flexDirection: 'row',margin:10, alignItems: 'center',  }}>
                                    <TouchableOpacity
                                    // onPress={() => { this.navigateScreen() }}
                                    >
                                        <Image source={require('../assests/twittermenu.png')} style={[{ width: 25, height: 20,tintColor:'grey', marginLeft: 10 },{transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]}]}></Image>
                                    </TouchableOpacity>
                                    {/* <Text style={{ fontSize: 22, color: 'white', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>{language.used_andavailablelimits}</Text> */}
                                    <Text style={{ fontSize: 22, color: '#1DA1F2', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>Home</Text>
                                    <TouchableOpacity style= {{position:'absolute',right:20,}}
                                    onPress={() => this.isSearch()}
                                    >
                                      { this.state.isSearch ?
                                        <Image source={require('../assests/close.png')} style={[{ width: 15, height: 15,tintColor:'grey',resizeMode:'contain', marginLeft: 10 },{transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]}]}></Image>
                                        :
                                        <Image source={require('../assests/search.png')} style={[{ width: 25, height: 20,tintColor:'grey',resizeMode:'contain', marginLeft: 10 },{transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]}]}></Image>
                                      }
                                        </TouchableOpacity>
                                </View>

                            </View>

                            { this.state.isSearch ?

                                <View style={{height:50,width:'95%',left:'2%',borderWidth:1,borderColor:'#C7D0F1',marginTop:10,borderRadius: 20}}>
                                    <SearchBar  
                                        containerStyle= {{
                                            backgroundColor: 'transparent',
                                            borderRadius: 20,
                                            borderTopColor:'transparent',
                                            borderBottomColor:'transparent',bottom:9
                                        }}
                                        inputContainerStyle= {{
                                            backgroundColor: 'white',
                                            borderRadius:20,
                                            borderTopColor:'white',
                                        }}
                                        round
                                        searchIcon={{ size: 24 }}
                                        onChangeText={text => this.SearchFilterFunction(text)}
                                        onClear={text => this.SearchFilterFunction('')}
                                        placeholder="Search"
                                        autoFocus= {true}
                                        value={this.state.search}
                                    />
                                </View>

                            :

                                null

                            }

                            {/* <TouchableOpacity onPress={() => this.pageCount()}>
                              <Text style={{marginLeft:10,marginTop:15,fontSize:18,fontFamily:Fonts.QuickSand_Bold,color:'#1DA1F2'}}>Trends</Text>
                            </TouchableOpacity> */}

                            { this.state.UserSubscriptionsArray != null ?

                            <FlatList
                                data={this.state.UserSubscriptionsArray}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                extraData={
                                    this.state
                                  }
                                renderItem={
                                    ({ item,index }) => { 
                                      // alert(item)
                                       return (
                                        <View style={styles.View1}>
                                            <TouchableOpacity
                                            onPress={() => { 
                                              // alert(index)
                                               this.setState({ currentIndex: index })
                                             this.isFilter(item,index)
                                           }}
                                            // onPress={() => this.handleSelection(item.id)}
                                            style={[ {  }]} 
                                            >
                                            <View style={[ styles.selected, { backgroundColor: index === currentIndex ? '#1DA1F2' : 'transparent' }]}>
                                                <Text style={[ styles.TextStyle1, { color: index === currentIndex ? '#FFF' : 'black' }]}>{this.topicName(item,index)}</Text>
                                            </View>
                                            </TouchableOpacity>

                                        </View>
                                        )}
                                        }
                                keyExtractor={item => item.id}
                                // ListEmptyComponent={this.state.isLoading ? <ActivityIndicator style={{alignItems:'center', top:SCREEN_WIDTH/2}} color='#016FA6' size='large'/> : this.render_FlatList_EmptyView()}
                            />

                            :
                                null
                            }

                            <Text style={{width:'100%',textDecorationLine: 'line-through',height:1,backgroundColor:'#D7DDF3',}}></Text>

                            </View>
                    {/* </LinearGradient> */}
                    <View style={{ flexDirection: 'column', flex: 1 }}>


                        <FlatList
                            // data={ this.state.isFilter ? this.state.FilterArray : this.state.dataSource }
                            data={ search.length > 0 ? searchData : this.state.tweetsArray }
                            style={{ marginTop: 0 }}
                            // maxToRenderPerBatch={10}

                            renderItem={
                                ({ item }) =>
                                <View style={{flex:1,paddingLeft:8,margin:5,alignItems:'center',}}>
                                  <View style = {{flexDirection:'column',width:'100%',alignItems:'center'}}>
                                    <View style = {{width:'100%',flexDirection:'row',alignItems:'center'}}>
                                        <View style={{ height: 70, width: 70,   alignItems: 'center', borderRadius: 40, backgroundColor: 'white', marginLeft: 0 }}>
                                            <Image source={require('./../assests/male.png')} style={{ height: 70, width: 70, borderRadius: 40, left: 15, right: 15, marginLeft: -30 }}></Image>
                                        </View>
                                        <View style={{flexDirection:'column',marginLeft:10}}>
                                        <Text style={{ fontSize:16,textAlign:'left',maxWidth:'100%',fontFamily: Fonts.QuickSand_Bold,marginRight:100 }}>{item.tweetId}</Text>
                                        <Text numberOfLines={1} style={{ fontSize:14,maxWidth:'100%',textAlign:'left',fontFamily: Fonts.QuickSand,color:'grey',marginRight:100,bottom:6 }}>{item.twitterHandle}</Text>
                                        </View>
                                        {/* <TouchableOpacity style= {{position:'absolute',right:0,height:20,width:20}} onPress={() => this.touchtodisplay(item)}>
                                          <Image source={images.downImage} style={{height:15,width:15,position:'absolute',right:10,tintColor:'grey'}} />
                                        </TouchableOpacity> */}
                                    </View>
                                    <View style = {{flexDirection:'row',width:'100%',alignItems:'center'}}>
                                        {/* <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>twitterProfileLink : {item.twitterProfileLink}</Text> */}
                                        <Text style={{ fontSize:14,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand,paddingTop:5,paddingBottom:5 }}>{item.tweet}</Text>
                                        {/* <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>tweetedOn : {item.tweetedOn}</Text>
                                        <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>rating : {item.rating}</Text> */}
                                    </View>
                                    {/* <Image source={images.div} style={{ height: 1.5, width: '100%', marginTop: 10 }}></Image> */}
                                    {/* <TouchableOpacity
                                    style={{height:40,width:'100%',alignItems:'center',justifyContent:'center',borderRadius:5,marginTop:5,backgroundColor:'#1DA1F2'}}
                                    onPress={() => { alert('Report') }}
                                    > */}
                                    <TouchableOpacity 
                                      style={{height:40,width:'100%',alignItems:'center',justifyContent:'center',borderRadius:5,marginTop:5,backgroundColor:'#1DA1F2'}}
                                      onPress={() => { 
                                        Linking.openURL('https://twitter.com/'+item.twitterHandle+'/status/'+item.tweetId);
                                      }}
                                    >
                                        <Text style={{color:'#fff',fontFamily: Fonts.QuickSand_Bold,}}>Report Tweet</Text>
                                    </TouchableOpacity>
                                    <Text style={{width:'100%',textDecorationLine: 'line-through',height:1,backgroundColor:'#D7DDF3',marginTop: 10}}></Text>
                                  </View>
                                </View>
                            }
                            keyExtractor={item => item.id}
                            ListEmptyComponent={this.state.isLoading ? null : this.render_FlatList_EmptyView()}

                        />
                        <View style={{justifyContent:'space-between',flexDirection:'row',padding:10}}>
                          <TouchableOpacity style={{justifyContent:'center'}} onPress={() => this.decrementCount()}>
                          { this.state.isDecrement ?
                            <Image source = {require('../assests/decrement.png')} style = {{height: 35,width: 35,resizeMode:'contain'}} /> 
                            :
                            null
                          } 
                          </TouchableOpacity>

                          <TouchableOpacity style={{justifyContent:'center',}} onPress={() => this.incrementCount()}>
                          { this.state.isIncrement ?
                           null
                            :
                            <Image source = {require('../assests/increment.png')} style = {{height: 35,width: 35,resizeMode:'contain'}} />
                          }
                          </TouchableOpacity>
                        </View>
                        {/* :
                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontFamily: Fonts.QuickSand_Bold,fontSize:18}}>No data Found</Text>
                        </View>
                        } */}
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isSort}
                        onRequestClose={() => {
                        }}>
                        <TouchableOpacity
                        style={{ flex: 1, height: SCREEN_HEIGHT + 100, width: SCREEN_WIDTH, position: 'absolute', bottom: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
                        onPress={() => this.touchtohide()}
                        >
                        </TouchableOpacity>

                        {/* <Card style={{ width: SCREEN_WIDTH, bottom: 0, borderTopRadius: 20, position: 'absolute' }}> */}
                        <View style={{flexDirection:'row',alignItems:'center',margin: 10}}>
                            <Image source={require('../assests/Report-Tweet.png')} style = {{ height:20,width:20,tintColor:'grey' }} />
                            {/* <TouchableOpacity onPress={() => this.reportAPI()}> */}
                            <TouchableOpacity onPress={() => this.reportPage()}>
                            {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('ReportPage',{ }) }}> */}
                            <Text style = {{ fontSize: 18,fontFamily: Fonts.QuickSand_Semi_Bold,padding:10 }}>Report Tweet</Text>
                            </TouchableOpacity>
                        </View>
                        {/* </Card> */}
                    </Modal>
                </SafeAreaView>
            </View>

        )

    }
}

const styles = {
    container: {
        flex: 1
    },
    changeLanguageViewStyletoAr: {
        borderRadius: 20,
        height: 40,
        borderWidth: 1,
        borderColor: 'black',
        width: 100,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    sliderReal: {
        backgroundColor: '#119EC2',
        height: 30,
    },
    activityIndicatorContainer: {
        backgroundColor: "#fff",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    emptyView:
    {
        flex:1,
        height:SCREEN_HEIGHT,
        alignItems:'center',
        justifyContent:'center',
    },
    textStyle:
    {
        textAlign: 'left', 
        color: 'black',
        fontFamily: Fonts.QuickSand_Bold,
        fontSize: 16
    },
    View1 :
    {
      flex: 1,
      flexDirection: 'column',
      padding: 10,
      marginTop: 5,
      marginBottom :5,
      // backgroundColor:'orange'
    },
    selected:
    {
      alignSelf: 'center',
      borderColor:'#C7D0F1',
      borderWidth:1,
      borderRadius:15,
      alignItems: 'center',
      justifyContent: 'center'
    },
    TextStyle1:
    {
      fontFamily: Fonts.QuickSand,
      padding:10,
      fontSize:16
    }

}