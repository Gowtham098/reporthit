import React, { Component } from "react"
import {
  AppRegistry,
  Button,
  Dimensions,
  StyleSheet,
  Text,
  View,
  FlatList,
  Alert,
  TouchableOpacity } from "react-native"
  import { Fonts } from "../Class/utils/fonts";
  import Constant from './Constant'

  let SCREEN_HEIGHT = Dimensions.get('window').height;
let SCREEN_WIDTH = Dimensions.get('window').width;

  const DATA = [
    {
      "id": 1,
      "topicName": "#VMovie",
      "createdOn": "2020-09-10T00:00:00"
    },
    {
      "id": 2,
      "topicName": "#SushantSinghRajput",
      "createdOn": "2020-09-10T00:00:00"
    },
    {
      "id": 3,
      "topicName": "#Corona Virus 2019",
      "createdOn": "2020-09-10T00:00:00"
    }
  ];

  export default class Streams extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
          UserArray : [],
          SubscriptionsArray : [],
          UserSubscriptionsArray : [],
          isLoading: false,
          isSubscribe : true,

          //UserSubscriptionsData : this.props.UserSubscriptionsData,
      };

      this.UserSubscriptions()
    }

    componentDidMount()
    {
      const { navigation } = this.props;

      this.focusListener = navigation.addListener('willFocus', () => {
      })
      if(this.props.navigation.state.params)
      {

      }
    //   alert(JSON.stringify(this.state.UserSubscriptionsData))

    //   this.dataLoad()

    }

    dataLoad()
    {
        let arr = this.state.UserSubscriptionsData.map(( item, index)=> {
            item.isSubscribe = true
            return {...item}
          })
        //   alert(JSON.stringify(arr))
          this.setState({ UserSubscriptionsData : arr });
    }

    render_FlatList_EmptyView = () => {
 
      var emptyView = (
   
      <View style={styles.emptyView}>
   
        <Text style={styles.textStyle}>No Data Found</Text>
   
      </View>
   
      );
   
      return emptyView ;
   
  };

    tappedOnDetails =(item,index) =>
    {
      Alert.alert('','Do you want to unsubscribe this stream?',
        [
          {text: 'CANCEL', onPress: () => console.log('Cancel Pressed')},
          {text: 'UNSUBSCRIBE', onPress:() => this.deleteAlert(item,index)},
        ],
        { cancelable: true } 
      )
    }

    deleteAlert = (item,index) =>
    {
      // alert(item.id)
      global.streamIndex = item.id
      this.unSubscribeAPI()
        // let arr = this.state.UserSubscriptionsData.map(( item, index)=> {
        //     if( ind == index )
        //     {
        //         item.isSubscribe = !item.isSubscribe
        //     }

        //     return {...item}
        //   })
            // var array = arr

            // array.splice(ind,1);
        //   alert(JSON.stringify(array))
        //   this.setState({ UserSubscriptionsData : array });
    }

    UserSubscriptions()
    {
        // console.log('https://6064-40280.el-alt.com/api/UserSubscriptions')

        let params = JSON.stringify({
            "userId": global.userRegisteredID
            // "userId": 25
          })
          // alert(global.userRegisteredID)

        fetch('https://6064-40280.el-alt.com/api/UserSubscriptions', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson.responseData))
          this.setState({ UserSubscriptionsArray : responseJson.responseData, isLoading: false })
          this.insertUserStreamsData(this.state.UserSubscriptionsArray)


        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    insertUserStreamsData(userStreamobj)
    {
      // alert(JSON.stringify(userStreamobj))
      userStreamobj.map((item)=>{
        this.checkUserStreams(item)
      })    
    }

    checkUserStreams(item)
    {
      // alert(JSON.stringify(item.id))
        Constant.UserStreamsDb().find({id : item.id}, function (err, docs)
        {
          // alert(JSON.stringify(docs))
            // console.log('docs',docs)

            if(docs.length>0)
            {
                Constant.UserStreamsDb().update({ id :item.id }, item, {}, function (err, numReplaced) {
            
                    // console.log('number emp',numReplaced)
                    // alert(numReplaced)
                });
            }
            else
            {
                Constant.UserStreamsDb().insert((item),function(err,newDocs){
                  // console.log('inserted emp',newDocs)
                });
            }
        });
    }

    unSubscribeAPI()
    {
      // console.log(Endpoints.DEV_URL+'/api/SubscribeStream);
      // return fetch(Endpoints.DEV_URL+'/api/SubscribeStream?nid='+this.state.National_ID+'&oldpassword='+this.state.oldPassword+'&newpassword='+this.state.newPassword)

      fetch('https://6064-40280.el-alt.com/api/UnSubscribeStream', {

        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "userId": global.userRegisteredID,
          "topicId": global.streamIndex,
        })

      })

      .then((response) => response.json())
      .then((responseJson) => {

        if (responseJson.responseCode == "Success")
        {
          alert(JSON.stringify(responseJson.responseMessage))
        //   this.setState({ SubscriptionsArray : responseJson.responseData, isLoading: false })
        }
        else if(responseJson.success == "false")
        {
          console.log(responseJson.responseMessage)
        }
        else
        {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    render() {
        // let UserSubscriptionsData = this.props.UserSubscriptionsData
        return(
            <View>

          <FlatList
            data={ this.state.UserSubscriptionsArray }
            // data={this.state.dataSource}
            extraData = { this.state }

            renderItem={
              ({ item,index }) =>
              <TouchableOpacity
              // onPress={() => { this.props.navigation.navigate('AdviceDetails',{AdviceDetails : item}) }}
              activeOpacity={0.8}
              style={{
                  flex: 1, flexDirection: 'row', borderRadius: 15, backgroundColor: 'white', padding: 10, margin: 10, shadowOffset: { height: 2, width: 2 },
                  shadowOpacity: 2,
                  shadowColor:'#d3d3d3',
                  shadowRadius: 5,
                  elevation: 5,
              }}>
                <View style={{flex:1,paddingLeft:8,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                    {/* <Text style={{ fontSize:16,width:'60%'}}>Hello :</Text> */}
                    <View style = {{width: '68%',}}>
                        <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>{item.topicName}</Text>
                    </View>
                    <TouchableOpacity
                        // onPress={() => { alert('1')}}
                        onPress={()=> this.tappedOnDetails(item,index)}
                        style={{height:40,width:'32%',alignItems:'center',justifyContent:'center'}}
                    >
                        {/* { item.isSubscribe ? */}
                        <View style={{height:40,width:'100%',borderRadius:20,backgroundColor:'#8391C5',alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'#fff',fontSize:16,fontFamily: Fonts.QuickSand_Semi_Bold}}>UnSubscribe</Text>
                        </View>
                        {/* :
                        <View style={{height:40,width:'100%',borderRadius:20,backgroundColor:'#456BF5',alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'#fff',fontSize:16,fontFamily: Fonts.QuickSand_Semi_Bold}}>Subscribe</Text>
                        </View>
                    } */}
                    </TouchableOpacity>
                </View>
              </TouchableOpacity>

            }
            keyExtractor={item => item.id}
            ListEmptyComponent={this.props.isLoading ? null : this.render_FlatList_EmptyView()}
          />

        </View>
        )
    }
}

const styles = StyleSheet.create({
  emptyView:
  {
      flex:1,
      height:SCREEN_HEIGHT,
      alignItems:'center',
      justifyContent:'center',
  },
  textStyle:
  {
      textAlign: 'left', 
      color: 'black',
      fontFamily: Fonts.QuickSand_Bold,
      fontSize: 16
  }
})