// import { View } from 'native-base';
import {
    AppRegistry,
    Button,
    Dimensions,
    View,
    StyleSheet,
    Text,
    Linking,
    Modal,
    FlatList,
    Image,
    ImageBackground,
    StatusBar,
    TouchableOpacity } from "react-native"
    import { Fonts } from "../Class/utils/fonts";
import React, { Component } from 'react';
import Dots from 'react-native-dots-pagination';
import Constant from "./Constant";
import { TextInput, Card } from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient'
 
let SCREEN_HEIGHT = Dimensions.get('window').height;

let SCREEN_WIDTH = Dimensions.get('window').width;
 
export default class Example extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 0,
      tweetsArray : [],
      totalTweetsCount : global.totalTweetsCount,
      isLoading: false,
      topicDetails : {},
      topicId : '',
      isSort: false,

      isDecrement : false,
      isIncrement : false,

      pages: 0,
      currentPage: 0,
      pageNumber : 1,
    }
  }

  componentDidMount()
  {
      if (this.props.navigation.state.params)
        {
            let topicDetails = this.props.navigation.state.params.topicDetails
            // alert(JSON.stringify(topicDetails))
            this.setState({ topicDetails: topicDetails })
            // alert(JSON.stringify(this.state.topicDetails))

            let topicId = this.props.navigation.state.params.topicId
            // alert(JSON.stringify(topicId))
            global.topicId = topicId
            this.setState({ topicId: topicId })
            // alert(JSON.stringify(this.state.topicId))
        }
        this.GetTweetsAPI()
        // alert(this.state.totalTweetsCount)
  }

  reportAPI(itemData)
    {
        // console.log('https://6064-40280.el-alt.com/api/GetTweetsByTopicId')
        this.setState({ isLoading: true })

        let params = JSON.stringify({
          "userId": global.userRegisteredID ,
          "tweetId": itemData.tweetId
          })
          // alert(global.userRegisteredID )
        //   alert(global.tweetId )
        // alert(params)

        fetch('https://6064-40280.el-alt.com/api/ReportAbuse', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson.responseMessage))
          this.setState({ isSort: false, isLoading: false })

        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

  GetTweetsAPI()
    {
        // console.log('https://6064-40280.el-alt.com/api/GetTweetsByTopicId')
        this.setState({ isLoading: true })

        let params = JSON.stringify({
            "page": this.state.pageNumber, 
            "perpage": 10,
            "query": "",
            // "topicId": 1,
            "topicId": global.topicId ,
          })
        //   alert(global.topicId )

        fetch('https://6064-40280.el-alt.com/api/GetTweetsByTopicId', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson))
          this.setState({ tweetsArray : responseJson.responseData,totalTweetsCount : responseJson.totalTweetsCount, isLoading: false })
          global.totalTweetsCount = responseJson.totalTweetsCount
          // alert(global.totalTweetsCount)

        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    incrementCount()
    {
      var count = global.totalTweetsCount
      count = count / 10;
      var data = Constant.round(count)
      // alert(data)
      if(data >  this.state.pageNumber)
      {
        this.setState ({
          pageNumber: this.state.pageNumber + 1,
          isDecrement : true
        });
      }
      else{
        this.setState ({
          isIncrement : true,
        });
      }
      // global.pageNumber = this.state.pageNumber
      this.GetTweetsAPI()
      // alert(this.state.pageNumber)
    }

    decrementCount()
    {
      var count = global.totalTweetsCount
      count = count / 10;
      var data = Constant.round(count)
      // alert(data)
      if(data <= this.state.pageNumber)
      {
        // alert(this.state.pageNumber)
        this.setState({ pageNumber : this.state.pageNumber + 1, isIncrement : false})
        // alert(this.state.pageNumber)
    }
    else{
      this.setState ({
        isDecrement : false,
      });
    }
    // global.pageNumber = this.state.pageNumber
      // alert(this.state.pageNumber)
      this.GetTweetsAPI()
    }

    touchtohide = () => {
      this.setState({ isSort: false })
    }
  
    touchtodisplay = (item) => {
      // alert(JSON.stringify(item.tweetId))
      global.tweetId = item.tweetId
      this.setState({ isSort: true })
    }

    increment()
    {
      let array = this.state.totalTweetsCount
      // alert(global.count)
      
      if(global.count <= array)
      {
        global.count = global.count + 1
      // alert(global.count)
      this.PageCount()
      }

      // array = array + 1
      // this.setState({totalTweetsCount : array})
      // alert(this.state.totalTweetsCount)
    }

    reportPage()
    {
      this.setState({isSort:false})
       this.props.navigation.navigate('ReportPage',{ }) 
    }

    PageCount()
    {
      let count = this.state.totalTweetsCount
      // alert(count)
      count = count / 5
      // alert(parseFloat(count))
      // var data = Math.round(count)
      // alert(count)
      if( count >= 0 && count <= 1 )
      {
        count = 1
      }
      else if( count > 1 && count <= 2 )
      {
        count = 2
      }
      global.count = count
      // alert(global.count)
      return count
    }
  
  render() {
    return (
        <View style={{flex:1}}>
          <View style={{height:'100%'}}>
            <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />
            <View style={{ height: 80, }}>
                <View style={{ flexDirection: 'row', height: 80,borderBottomWidth : 1,borderBottomColor:'#E9E8E8',alignItems:'center',justifyContent:'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                      <TouchableOpacity onPress={()=>{this.props.navigation.goBack(null)}}>
                        <Image source={require('../assests/back.png')} style={[{ width: 25,tintColor:'#1DA1F2',resizeMode:'contain', height: 20, marginLeft: 10 },]}></Image>
                        {/* <ImageBackground source={images.back} style={[{ width: 25, height: 20,marginTop:2, marginLeft: 10 }]}></ImageBackground> */}
                      </TouchableOpacity>
                      <Text style={{ fontSize: 22, color: '#1DA1F2', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>{this.state.topicDetails.topicName}</Text>
                    </View>          
                  </View>
            </View>

            {/* <Dots length={1} active={this.state.active} /> */}
            {this.state.isLoading ? Constant.showLoader() : null}

            <FlatList
            data={ this.state.tweetsArray }
            extraData={this.state}
            contentContainerStyle={{ paddingBottom: 20}}
            // data={this.state.dataSource}

            renderItem={
              ({ item,index }) =>
              <View style={{flex:1,paddingLeft:8,margin:5,alignItems:'center',}}>
                                  <View style = {{flexDirection:'column',width:'100%',alignItems:'center'}}>
                                    <View style = {{width:'100%',flexDirection:'row',alignItems:'center'}}>
                                        <View style={{ height: 70, width: 70,   alignItems: 'center', borderRadius: 40, backgroundColor: 'white', marginLeft: 0 }}>
                                            <Image source={require('../assests/male.png')} style={{ height: 70, width: 70, borderRadius: 40, left: 15, right: 15, marginLeft: -30 }}></Image>
                                        </View>
                                        <View style={{flexDirection:'column',marginLeft:10}}>
                                        <Text style={{ fontSize:16,textAlign:'left',maxWidth:'100%',fontFamily: Fonts.QuickSand_Bold,marginRight:100 }}>{item.tweetId}</Text>
                                        <Text numberOfLines={1} style={{ fontSize:14,maxWidth:'100%',textAlign:'left',fontFamily: Fonts.QuickSand,color:'grey',marginRight:100,bottom:6 }}>{item.twitterHandle}</Text>
                                        </View>
                                        {/* <TouchableOpacity style= {{position:'absolute',right:0,height:20,width:20}} onPress={() => this.touchtodisplay(item)}>
                                          <Image source={images.downImage} style={{height:15,width:15,position:'absolute',right:10,tintColor:'grey'}} />
                                        </TouchableOpacity> */}
                                    </View>
                                    <View style = {{flexDirection:'row',width:'100%',alignItems:'center'}}>
                                        {/* <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>twitterProfileLink : {item.twitterProfileLink}</Text> */}
                                        <Text style={{ fontSize:14,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand,paddingTop:5,paddingBottom:5 }}>{item.tweet}</Text>
                                        {/* <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>tweetedOn : {item.tweetedOn}</Text>
                                        <Text style={{ fontSize:18,width:'100%',textAlign:'left',fontFamily: Fonts.QuickSand_Semi_Bold }}>rating : {item.rating}</Text> */}
                                    </View>
                                    {/* <Image source={images.div} style={{ height: 1.5, width: '100%', marginTop: 10 }}></Image> */}
                                    {/* <TouchableOpacity
                                    style={{height:40,width:'100%',alignItems:'center',justifyContent:'center',borderRadius:5,marginTop:5,backgroundColor:'#1DA1F2'}}
                                    onPress={() => { alert('Report') }}
                                    > */}
                                    <TouchableOpacity 
                                      style={{height:40,width:'100%',alignItems:'center',justifyContent:'center',borderRadius:5,marginTop:5,backgroundColor:'#1DA1F2'}}
                                      // onPress={() => this.reportAPI(item,index)}
                                      onPress={() => {
                                        this.reportAPI(item,index)
                                        Linking.openURL('https://twitter.com/'+item.twitterHandle+'/status/'+item.tweetId);
                                      }}
                                    >
                                        <Text style={{color:'#fff',fontFamily: Fonts.QuickSand_Bold,}}>Report Tweet</Text>
                                    </TouchableOpacity>
                                    <Text style={{width:'100%',textDecorationLine: 'line-through',height:1,backgroundColor:'#D7DDF3',marginTop: 10}}></Text>
                                  </View>
                                </View>

            }
            keyExtractor={item => item.id}
          />
         <View style={{justifyContent:'space-between',flexDirection:'row',padding:10}}>
          <TouchableOpacity style={{justifyContent:'center'}} onPress={() => this.decrementCount()}>
            { this.state.isDecrement ?
              <Image source = {require('../assests/decrement.png')} style = {{height: 35,width: 35,resizeMode:'contain'}} /> 
            :
              null
            } 
          </TouchableOpacity>

          <TouchableOpacity style={{justifyContent:'center',}} onPress={() => this.incrementCount()}>
            { this.state.isIncrement ?
              null
            :
              <Image source = {require('../assests/increment.png')} style = {{height: 35,width: 35,resizeMode:'contain'}} />
            }
          </TouchableOpacity>
        </View>
          </View>
          <View style={{flexDirection:'row',width:'100%',position:'absolute',bottom:5,alignItems:'center',justifyContent:'center',}}>
          {/* <TouchableOpacity style={{ height: 50, borderRadius: 31,}}
            // onPress={() => { alert('1') }}
            >
              <LinearGradient colors={['#0296CC', '#016FA6', '#004B82']} style={{ width: '100%',left:'5%', flexDrection: 'row', height: 50, borderRadius: 31 }}>
                <View style={{ flexDrection: 'row', height: 50, width: '100%', borderRadius: 31, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ flexDirection: 'row', fontSize: 16, color: 'white', fontFamily: Fonts.QuickSand_Semi_Bold}}>  Prev page  </Text>
                </View>
              </LinearGradient>
            </TouchableOpacity> */}

            {/* <Text style={{fontFamily:Fonts.QuickSand_Semi_Bold}}>{global.pageNumber} of {Constant.round(global.totalTweetsCount/10)}</Text> */}

            {/* <TouchableOpacity style={{ height: 50, borderRadius: 31,}}
            onPress={() => { this.increment() }}
            >
              <LinearGradient colors={['#0296CC', '#016FA6', '#004B82']} style={{ width: '100%',right:'5%', flexDrection: 'row', height: 50,borderRadius: 31 }}>
                <View style={{ flexDrection: 'row', height: 50, width: '100%', borderRadius: 31, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ flexDirection: 'row', fontSize: 16, color: 'white', fontFamily: Fonts.QuickSand_Semi_Bold}}>  Next page  </Text>
                </View>
              </LinearGradient>
            </TouchableOpacity> */}
            </View>

          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.isSort}
            onRequestClose={() => {
            }}>
            <TouchableOpacity
              style={{ flex: 1, height: SCREEN_HEIGHT + 100, width: SCREEN_WIDTH, position: 'absolute', bottom: 0, backgroundColor: 'rgba(0,0,0,0.5)' }}
              onPress={() => this.touchtohide()}
            >
            </TouchableOpacity>

            <Card style={{ width: SCREEN_WIDTH, bottom: 0, borderTopRadius: 20, position: 'absolute' }}>
              <View style={{flexDirection:'row',alignItems:'center',margin: 10}}>
                <Image source={require('../assests/Report-Tweet.png')} style = {{ height:20,width:20,tintColor:'grey' }} />
                {/* <TouchableOpacity onPress={() => this.reportAPI()}> */}
                <TouchableOpacity onPress={() => this.reportPage()}>
                {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('ReportPage',{ }) }}> */}
                <Text style = {{ fontSize: 18,fontFamily: Fonts.QuickSand_Semi_Bold,padding:10 }}>Report Tweet</Text>
                </TouchableOpacity>
              </View>
            </Card>
          </Modal>

        </View>
    )
  }
}