import { StyleSheet, View, Text,I18nManager, Dimensions, ImageBackground, ActivityIndicator,StatusBar, Image } from 'react-native';
import React, { Component } from 'react';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'react-native-best-viewpager';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { HeaderBackButton } from 'react-navigation-stack';
import { SafeAreaView } from 'react-native-safe-area-context';
import UserStreams from '../Class/UserStreams'
import AllStreams from '../Class/AllStreams'
import { withNavigation  } from 'react-navigation';
import { Fonts } from './utils/fonts'

import Realm from 'realm';
let realm;

let SCREEN_HEIGHT = Dimensions.get('window').height;
let SCREEN_WIDTH = Dimensions.get('window').width;

class ViewPagerPage extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Claims',
      headerTintColor: '#ffffff',
      headerLeft: <HeaderBackButton tintColor='#fff' onPress={() => navigation.goBack(null)} />,

      headerStyle:
      {
        backgroundColor: "#292bca",
      },
      headerBackTitle: null,
      headerTitleStyle:
      {
        fontSize: 16
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
        BoolValue: '',
        OsClaimsArray: [],
        paidClaimsArray: [],
        ProfileArray: [],
        reimbursementArray :[],
        isLoading: false,

        GetStreamssArray : [],
        UserSubscriptionsArray : [],
    };
    
    this.AddUser()
    this.getStreamsFromApi()
    this.UserSubscriptions()
  }

  componentDidMount()
  {
    const { navigation } = this.props;
       
       
      this.focusListener = navigation.addListener('willFocus', () => {
      })
      if (this.props.navigation.state.params) {
        let StreamsData = this.props.navigation.state.params.StreamsData
        // this.setState({ GetStreamssArray: StreamsData })
        // alert(JSON.stringify(StreamsData))
    }


      // this.AddUser()
      // this.getStreamsFromApi()
      // this.UserSubscriptions()

  }

  AddUser()
    {
        // console.log(https://6064-40280.el-alt.com/api/AddUser);

        let params = JSON.stringify({

            "authToken": global.userData.authToken,
            "authTokenSecret": global.userData.authTokenSecret,
            "email": global.userData.email,
            "name": global.userData.name,
            "twitterUserID": global.userData.userID,
            "userName": global.userData.userName,
          })
        //   alert(params)

        fetch('https://6064-40280.el-alt.com/api/AddUser', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then(response => response.json())

      .then(responseJson => {
        //   alert("hlo")

          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson.responseData))
          this.setState({ isLoading: false })
          global.userRegisteredID = responseJson.responseData

        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }

      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }

    
    fectchStreamsData()
    {
      realm = new Realm({ path: 'UserDatabase.realm' });
        var streamsData = realm.objects('AllStreamsData');
        this.state = {
          GetStreamssArray: streamsData,
        };

        // alert(JSON.stringify(this.state.GetStreamssArray))
    }


    getStreamsFromApi()
    {
      return fetch('https://6064-40280.el-alt.com/api/GetStreams')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({isLoading:false})

        if (responseJson.responseCode == "Success")
        {
          // alert(JSON.stringify(responseJson))
          this.setState({ GetStreamssArray : responseJson.responseData, isLoading: false })
          // alert(JSON.stringify(this.state.GetStreamssArray))
        }
        else if(responseJson.success == "false")
        {
          console.log(responseJson.responseMessage)
        }
        else
        {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        console.log(error);
      });
    }


    UserSubscriptions()
    {
        // console.log('https://6064-40280.el-alt.com/api/UserSubscriptions')

        let params = JSON.stringify({
            "userId": global.userRegisteredID
            // "userId": 25
          })
          // alert(global.userRegisteredID)

        fetch('https://6064-40280.el-alt.com/api/UserSubscriptions', {

          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: params

        })

        .then((response) => response.json())
        .then((responseJson) => {
            
          if (responseJson.responseCode == "Success") {

            // alert(JSON.stringify(responseJson))
          this.setState({ UserSubscriptionsArray : responseJson.responseData, isLoading: false })


        } else if(responseJson.success == "false") {

            console.log(responseJson.responseMessage)

        } else {
          // alert("Invalid Credentials")
        }
      })
      .catch((error) => {
        this.setState({isLoading:false})
        console.log(JSON.stringify(error));
      });
    }




  navigateScreen()
  {
    this.props.navigation.goBack(null)
  }

  render()
  {

    return (
      <View style={{ flex: 1,width:SCREEN_WIDTH,height:SCREEN_HEIGHT }}>
       <SafeAreaView edges={[ 'bottom',]} style={{flex:0,backgroundColor:'#1DA1F2'}}/>
       <StatusBar backgroundColor="#E9E8E8" barStyle="dark-content" />

        <SafeAreaView edges={['right', 'bottom', 'left']} style={{ flex: 1,backgroundColor:'#fff'}}>
        {this.state.isLoading ? Constant.showLoader() : null}
        <View style={{ height: 80, flexDirection: 'column', backgroundColor: '#fff', borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}>
          <View style={{ flexDirection: 'row', height: 80 }}>
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', padding: 10, }}>
              <View style={{ flex: 0.8, flexDirection: 'row', marginTop: 5, alignItems: 'center' }}>
                <TouchableOpacity
                  // onPress={() => { this.navigateScreen() }}
                >
                  <ImageBackground source={require('../assests/notification.png')} tintColor={'#1DA1F2'} resizeMode={'contain'} style={[{ width: 25, height: 20, marginLeft: 10,resizeMode: 'contain', },{transform: [{scaleX: I18nManager.isRTL ? -1 : 1}]}]}></ImageBackground>
                </TouchableOpacity>
                {/* <Text style={{ fontSize: 22, color: 'white', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>{language.claims}</Text> */}
                <Text style={{ fontSize: 22, color: '#1DA1F2', marginLeft: 20, fontFamily: Fonts.QuickSand_Bold }}>Streams</Text>

              </View>
              <View style={{ flex: 0.2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 10 }}>
              </View>


            </View>
          </View>
        </View>
        <IndicatorViewPager
          initialPage={global.pageId != null ? 0 : global.pageId}
          indicatorPosition="top"
          style={{ flex: 1, flexDirection: 'column-reverse', paddingTop: 0, backgroundColor: 'white' }}
          indicator={this._renderTitleIndicator()}
        >
          <View style={{}}>
            <AllStreams
              navigation={this.props.navigation}
              isLoading={this.state.isLoading}

              GetStreamsData = { this.state.GetStreamssArray }

            />
          </View>
          <View style={{}}>
            <UserStreams
              {...this.props}
              isLoading = {this.state.isLoading}
              OsClaimsData= {this.state.reimbursementArray}

              UserSubscriptionsData= {this.state.UserSubscriptionsArray}
              
            />
          </View>
        </IndicatorViewPager>
        </SafeAreaView>
      </View>
    );
  }
  _renderTitleIndicator() {
    return <PagerTitleIndicator
      style={{ backgroundColor: "#fff", borderBottomWidth: 0.8,borderBottomColor: '#E9E8E8', height: 50 }}
      selectedItemTextStyle={{ padding: 16, height: 60, color: '#1DA1F2', textAlign: 'center', backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center',borderBottomWidth: 4, borderBottomColor: '#6DBAE1', fontFamily: Fonts.QuickSand_Bold, fontSize: 14, width: (SCREEN_WIDTH / 2) }}
      itemStyle={{ padding: 15, height: 50, color: '#1DA1F2', textAlign: 'center', justifyContent: 'center', alignItems: 'center', fontFamily: Fonts.QuickSand_Bold, width: (SCREEN_WIDTH / 2) }}
      selectedBorderStyle={{ borderBottomWidth: 3,borderBottomColor: '#1DA1F2', backgroundColor: 'transparent', borderLeftWidth: 0.19, borderLeftColor: '#1DA1F2' }}
      itemTextStyle={{ justifyContent: 'center', alignItems: 'center', fontFamily: Fonts.QuickSand_Bold, color: '#1DA1F2', fontSize: 14, }}
      titles={["All Streams", "My Streams"]}
    />;
  }

  _renderDotIndicator() {
    return <PagerDotIndicator pageCount={3} />;
  }
}

const styles = StyleSheet.create({
  changeLanguageViewStyletoAr: {
    borderRadius: 20,
    height: 40,
    borderWidth: 1,
    borderColor: 'black',
    width: 100,
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
})

     export default withNavigation(ViewPagerPage);