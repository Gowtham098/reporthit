var Realm = require('realm');

const CarSchema = {
  name: 'user_details',
//   primaryKey:'user_id',
  properties: {
    // id:  'int?',
    user_id: 'int?',
    user_twitter_id : 'string?',
    user_name: 'string?',
    user_handle: 'string?',
    user_email: 'string?',
    authToken : 'string?',
    authTokenSecret : 'string?'
  }
};
